import React, { Component } from 'react';
import { AppRegistry } from 'react-native';
import MainApp from './app/index';

AppRegistry.registerComponent('commonStructure', () => MainApp);
