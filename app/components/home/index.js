import React, { Component } from 'react';
import { StyleSheet, Platform, View, Text  } from 'react-native';

//common import
import { connect } from 'react-redux';
import { ActionCreators } from '../../actions'
import { bindActionCreators } from 'redux'
// end of common import

//importing header Component
import HeaderRightIcon from '../header/HeaderRightIcon';
import HeaderLeftIcon from '../header/HeaderLeftIcon';
//end header component

class Home extends React.Component {
  static navigationOptions = (navigation) => ({
    headerTitle:'Home',
    headerLeft: (<HeaderLeftIcon icon={'left-arrow'} {...navigation} back={'Car'} />),
    headerRight: (<HeaderRightIcon  icon={'inbox'} screen={'Car'} {...navigation} />)
  })

  render () {
    return(<View><Text>Home</Text></View>)
  }
}



function mapDispatchToProps(dispatch) {
  return Object.assign({dispatch: dispatch}, bindActionCreators(ActionCreators, dispatch));
}

const mapStateToProps = state => {
  return {
    navReducer: state.navReducer,
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
