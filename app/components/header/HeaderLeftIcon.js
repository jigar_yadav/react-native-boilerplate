import React from 'react';
import { TouchableOpacity } from 'react-native';
import moment from 'moment';

import { connect } from 'react-redux';
import { ActionCreators } from '../../actions';
import { bindActionCreators } from 'redux';

import Styles from './style';
import CustomIcon from '../icon/svgIcon';

class HeaderLeftIcon extends React.Component {
  constructor() {
    super();
    this.onPress = this.onPress.bind(this);
  }

  onPress() {
    var self = this;
    if (this.props.screen == 'Inbox') {
      let data = {
        fromDate: moment('2017-07-30T08:03:16.019Z').toISOString(),
        toDate: moment().toISOString(),
      }
      this.props.getConversations(data);
    }
    setTimeout(function () {
      self.props.navigation.goBack(null);
    }, 0);
    // this.props.navigate(this.props.back);
  }

  render () {
    const { icon } = this.props;
    return (
      <TouchableOpacity style={ Styles.headerLeftContainer } onPress={this.onPress}>
        <CustomIcon name={icon} />
      </TouchableOpacity>
    )
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

const mapStateToProps = state => {
  return {
    navReducer: state.navReducer,
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(HeaderLeftIcon);
