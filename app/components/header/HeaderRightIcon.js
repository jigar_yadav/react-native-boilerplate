import React from 'react';
import { TouchableOpacity,View,Text } from 'react-native';
import moment from 'moment';

import { connect } from 'react-redux';
import { ActionCreators } from '../../actions';
import { bindActionCreators } from 'redux';

import Styles from './style';
import CustomIcon from '../icon/svgIcon';
import { addNavigationHelpers, NavigationActions } from 'react-navigation';

class HeaderRightIcon extends React.Component {
  constructor() {
    super();
    this.onPress = this.onPress.bind(this);
    this.state = {
      showNewMsgIcon:false
    }
  }

  onPress() {

  }

  render () {
    const { icon, text, msgCount } = this.props;
    return (

      <TouchableOpacity style={ Styles.headerRightContainer } onPress={this.onPress}>
        {
          icon ?
          <CustomIcon newMessage={this.state.showNewMsgIcon} name={icon=='inbox'?'new_msg':icon} />
          :
          <View/>
        }
        {
          text ?
          <Text style={Styles.headerRightText}>{this.props.text}</Text>
          :
          <View/>
        }
      </TouchableOpacity>
    )
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

const mapStateToProps = state => {
  return {
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(HeaderRightIcon);
