import { StyleSheet, Platform } from 'react-native';

export default Styles = StyleSheet.create({
  headerLeftContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: Platform.OS == 'android' ? 20 : 10,
    paddingLeft: 5
  },
  headerRightContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: Platform.OS == 'android' ? 20 : 10,
    paddingRight: 5,
  },
  headerRightText:{
    color:'#18bfe6',
    fontSize:17,
    fontWeight:'800',
    paddingRight: 20
  }
})
