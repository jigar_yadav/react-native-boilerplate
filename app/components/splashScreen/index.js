import React, { Component } from 'react';
import { StyleSheet, Platform, View, Text  } from 'react-native';

//common import
import { connect } from 'react-redux';
import { ActionCreators } from '../../actions'
import { bindActionCreators } from 'redux'
// end of common import


class SplashScreen extends React.Component {
  static navigationOptions = (navigation) => ({
    headerTitle:'SplashScreen',
    headerLeft:<Text>Left</Text>,
    headerRight:<Text>right</Text>
  })
  componentWillMount() {
    console.log(this.props);
  }
  render () {
    return(
      <View>
        <Text onPress={()=>{this.props.navigate('Home')}}>SplashScreen</Text>
      </View>
    )
  }
}

function mapDispatchToProps(dispatch) {
  return Object.assign({dispatch: dispatch}, bindActionCreators(ActionCreators, dispatch));
}

const mapStateToProps = state => {
  return {
    navReducer: state.navReducer,
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(SplashScreen);
