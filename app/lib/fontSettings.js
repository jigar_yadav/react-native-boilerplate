import { Platform } from 'react-native';

export default fonts = {
  regular: Platform.OS == 'android' ? 'Roboto-Regular' : 'Helvetica',
  bold: Platform.OS == 'android' ? 'Roboto-Bold' : 'Helvetica-Bold'
}
