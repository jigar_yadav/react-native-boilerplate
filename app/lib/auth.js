// import { AsyncStorage } from 'react-native';
// import * as types from '../actions/types';
// import Api from '../api/login';
// import Settings from './defaultSettings';
//
// export default function authMiddleware({ dispatch, getState }) {
//   return (next) => (action) => {
//     if (typeof action === 'function') {
//       let state = getState();
//       if (state.tokenReducer.internetStatus != Settings.internet.none) {
//         if(state.tokenReducer.token && isExpired(state.tokenReducer.token)) {
//           // make sure we are not already refreshing the token
//           if(!state.refreshTokenPromise) {
//             return refreshToken(dispatch, state).then(() => next(action));
//           } else {
//             return state.refreshTokenPromise.then(() => next(action));
//           }
//         }
//       }
//     }
//     return next(action);
//   }
// }
//
// function isExpired(token) {
//   let currentTime = new Date();
//   let expires_date = new Date(token.expires_date);
//   return currentTime > expires_date;
// }
//
// function refreshToken(dispatch, state) {
//   let refreshTokenPromise = Api.RefreshToken({
//     refresh_token: state.tokenReducer.token.refresh_token
//   }).then(resp => {
//     dispatch({
//       type: types.TOKEN_FOUND,
//       token: resp,
//       user: state.tokenReducer.user
//     });
//     return resp.access_token ? Promise.resolve(resp) : Promise.reject({
//       message: 'could not refresh token'
//     });
//   }).catch(ex => {
//     console.log('exception refresh_token', ex);
//     dispatch({
//       type: types.TOKEN_FOUND,
//       token: null,
//       user: null
//     });
//     dispatch({
//       type: types.RESET,
//       initialStateComesFrom: {navReducer: {index: 0, routes: [{routeName: "Login", key: "Init0"}]}}
//     })
//     AsyncStorage.setItem('token', '');
//   });
//
//   dispatch({
//     type: types.REFRESHING_TOKEN,
//     // we want to keep track of token promise in the state so that we don't try to refresh the token again while refreshing is in process
//     refreshTokenPromise
//   });
//
//   return refreshTokenPromise;
// }
