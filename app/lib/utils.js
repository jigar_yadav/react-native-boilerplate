import { AsyncStorage } from 'react-native';
import convert from 'convert-units';
import numeral from 'numeral';

const utils = {
  convertToKm:(val) => {
    var newKm = convert(val).from('m').to('km')
    if(newKm<10){
      newKm = numeral(newKm).format('0.0');
    }else{
      newKm = numeral(newKm).format('0');
    }
    return newKm + ' km';
  },

  storeState: (data) => {
    AsyncStorage.setItem('OBI_State', JSON.stringify(data));
  }
}

export default utils;
