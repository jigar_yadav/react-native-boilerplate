import React, { Component } from 'react';
import { StyleSheet, Platform, View, Text  } from 'react-native';
import { addNavigationHelpers, NavigationActions } from 'react-navigation';

//common import
import { connect } from 'react-redux';
import { ActionCreators } from '../actions'
import { bindActionCreators } from 'redux'
// end of common import

import MainStack from './MainStack';

class MainNavigation extends React.Component {
  render () {
    return(
      <View style={{flex:1}}>
        <MainStack
          navigation={addNavigationHelpers({
          dispatch: this.props.dispatch,
          state: this.props.navReducer,
        })}
        />
      </View>
    )
  }
}


function mapDispatchToProps(dispatch) {
  return Object.assign({dispatch: dispatch}, bindActionCreators(ActionCreators, dispatch));
}

const mapStateToProps = state => {
  return {
    navReducer: state.navReducer,
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(MainNavigation);
