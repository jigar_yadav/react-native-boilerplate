import React, { Component } from 'react';
import { StyleSheet, Platform, View, Text  } from 'react-native';
import { StackNavigator, TabNavigator, NavigationActions } from 'react-navigation';


//route imports
import SplashScreen from '../components/splashScreen';
import Home from '../components/home';

const navigationOptions = {
  headerStyle: {
    // borderBottomWidth: 1,
    // borderBottomColor: '#d6d6d6',
    height: 70,
    justifyContent: 'flex-end',
    elevation:1,
    // backgroundColor:'#fff'
  },
  headerTitleStyle: {
    color: '#484848',
    justifyContent: 'flex-end',
    alignSelf: 'center',
    marginTop: Platform.OS == 'android' ? 18 : 10,
    fontSize: 20
  },
  headerTintColor: '#484848',
};

const MainStack = StackNavigator({
  SplashScreen: {
    screen: SplashScreen,
  },
  Home:{
    screen:Home
  }
},{
  navigationOptions,
  initialRouteName: 'SplashScreen',
})


export default MainStack;
