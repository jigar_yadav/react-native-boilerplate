export default {
  'settings_header' : 'Settings',
  'settings_profile' : 'Profile details',
  'settings_vehicle' : 'Vehicle details',
  'settings_notifications' : 'Notifications',
  'settings_language' : 'Language',
  'settings_termsOfServices' : 'Terms of services',
  'settings_logout' : 'Log out',
  'settings_logoutPop': 'Log out of OBI+',
  'settings_cancel' : 'Cancel',

  'profileDetail_inputEmail' : 'Email',
  'profileDetail_inputPassword' : 'Password',
  'profileDetail_Save' : 'Save',

  'vehicleDetail_inputRegistrationDate' : 'Registration date',
  'vehicleDetail_inputNumberPlate' : 'Number plate',
  'vehicleDetail_inputVIN' : 'VIN',
  'vehicleDetail_inputModel' : 'Model',
  'vehicleDetail_inputProductionDate' : 'Production date',
  'vehicleDetail_inputEngineType' : 'Engine type',
  'vehicleDetail_inputEngineDisplacement' : 'Engine displacement',
  'vehicleDetail_inputGrarboxType' : 'Gearbox type',
  'termsOfServices_introduction' : 'Introduction',

  'journeys_header' : 'Journeys',
  'journeys_notFound': 'no journeys found',
  'journey_header' : 'Journey',
  'journey_distance' : 'Distance',
  'journey_duration' : 'Duration',
  'journey_fuelConsumption' : 'Fuel consumption',
  'journey_averageSpeed' : 'Avg. speed',
  'journey_notes' : 'Notes',
  'journey_daysAgo':'days ago',
  'journey_writeNotes' : 'Write notes',

  'car_header': 'Car',
  'car_mapHeaderText': 'Total passed distance',
  'car_cartTitle': 'Weekly distance',
  'car_condation': 'Condition',
  'car_fuelLevel': 'Fuel level',
  'car_batteryStatus' : 'Battery status',
  'car_totalMileage' : 'Total Mileage',
  'car_contaceServicePartners' : 'Contact service partners',
  'car_servicePartner' : 'Service partners',
  'car_AddNewServicePartner': 'Add new service partner',
  'car_repairLogBook' : 'Repair log-book',
  'car_AddNew' : 'Add new',
  'car_TitleRequire' : 'Title Require',
  'car_descriptionRequire' : 'Description Require',
  'car_nextService' : 'Next service in',
  'car_kmToNextSrvices' : 'Km to next service',
  'car_noDataFound' : 'No data found',
  'car_month': 'Month',
  'car_week': 'Week',
  'car_monthly' : 'Last 4 weeks',
  'car_weekly': 'Last 7 days',
  'car_yearly' : 'Last 12 months',
  'car_year': 'Year',
  'car_contactYourRepairShop': 'Contact your repair shop',
  'car_pending':'Pending',
  'car_summary': 'Summary',
  'car_possibleCauses' : 'Possible causes',
  'car_description' : 'Description',
  'car_vehicleNotFound': 'No vehical is associated with this user, you will not see any data.',
  'car_error':'error',
  'car_errors':'errors',
  'car_AddNewLog' : 'Add new log',
  'car_repLogBook': 'Rep. log-book',
  'car_EditLog' : 'Edit new log',
  'car_title' : 'Title',
  'car_Phone' : 'Phone',
  'car_Email' : 'Email',

  'stream_header' : 'Stream',

  'tab_stream': 'STREAM',
  'tab_car': 'CAR',
  'tab_journey' : 'JOURNEYS',
  'tab_spending': 'SPENDINGS',
  'tab_setting': 'SETTINGS',

  'forgotPassword_sendMeNewPassword': 'Send me new password',
  'forgotPassword_Email_Required' : 'Email is required.',
  'forgotPassword_Enter_Valid_Email' : 'Enter a valid email',
  'forgotPassword_PasswordRequired' : 'Password is required',

  'success_done': 'Done!',
  'success_summary1': ' We have sent you an email with',
  'success_summary2': ' a link to reset your password',
  'success_backToLogIn': 'Back to Log in',

  'error_ops':'Ups!',
  'error_summary1':'Something went wrong!',
  'error_summary2' : 'Please, try again',
  'error_error' : 'error',
  'alarms':'alarms',

  'notification_vehicleStatus' : 'Vehicle status',
  'notification_dtc': 'DTC codes',
  'notification_engine': 'Check engine light on ',
  'notification_lowBattery': 'Low battery voltage',
  'notification_HighTemp' : 'High temperature',
  'notification_drivingBehiviour' : 'Driving behaviour',
  'notification_harshAcceleration' : 'Harsh acceleration',
  'notification_harshBraking' : 'Harsh braking',
  'notification_highRPM' : 'High RPM',
  'notification_quickTurn' : 'Quick turn',
  'notification_fatigueDriving' : 'Fatigue driving',
  'notification_msgTitle' : 'OBI+ & partners',
  'notification_Tips' : 'Tips',
  'notification_newMessages' : 'New messages',
  'notification_others' : 'Others',
  'notification_deviceRemoved' : 'Device removed',
  'notification_wentWrong': 'Something went wrong',

  'spending_Spending' : 'Spendings',
  'spending_total_spending': 'Total spendings',
  'spending_addSpending': 'Add new spending',
  'spending_moneySpent': 'Money Spent',
  'spending_amount': 'Amount',
  'spending_amountRequire': 'Amount is required.',
  'spending_titleRequire': 'Title is required',
  'spending_delete': 'Delete spending?',
  'spending_phoneRequire': 'Phone is required.',

  'default_hardBraking' : 'Hard braking',
  'default_quickCornering' : 'Quick cornering',
  'default_hardAcceleration' : 'Hard acceleration',
  'default_fatigueDriving' : 'Fatigue driving',
  'default_high_RPM' : 'High RPM',
  'default_deviceUnplugged': 'OBI+ device unplugged',
  'default_checkEngineLight' : 'Check engine light on',
  'default_overheating' : 'Overheating',

  'noRepairsFound' : 'No repairs found.',
  'noContactsFound' : 'No contacts found.',
  'noSpendingsFound' : 'No spendings found.',
  'noInternetConnection' : 'No internet connection',

  'edit' : 'Edit',
  'delete' : 'Delete',
  'login_forgotPassword' : 'Forgotten password?',
  'login_login' : 'Log in',
  'loadMore' :'Load more',
  'writeNote' : 'Write a message'
};
