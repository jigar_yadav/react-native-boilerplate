import { AsyncStorage, Alert, View, ScrollView } from 'react-native';
import I18n, { getLanguages }from 'react-native-i18n';
import en from './locales/en';
import da from './locales/da';

// export const refreshLanguage = function(language) {
//   AsyncStorage.getItem('token').then((res) => {
//     if (res) {
//       data = JSON.parse(res);
//       I18n.locale = language
//       // console.log(data)
//     }
//   })
// }
//
// AsyncStorage.getItem('token').then((res) => {
//   if (res) {
//     data = JSON.parse(res);
//     console.log('in i18n file');
//     // console.log(data)
//     I18n.locale = data.language
//   }
// })

I18n.locale = 'da'

I18n.fallbacks = true;

I18n.translations = {
  en,
  da
};

export default I18n;
