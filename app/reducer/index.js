import { combineReducers } from 'redux';
import * as demoReducer from './demo';
import * as navReducer from './navigation';

export default combineReducers(Object.assign(
  demoReducer,
  navReducer
));
