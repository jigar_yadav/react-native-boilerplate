import createReducer from '../lib/createReducer'
import * as types from '../actions/types'

var initialstate = {
  data:0
}

export const demoReducer = createReducer(initialstate, {
  [types.DEMO](state, action){
    return Object.assign({}, state, {
      data:action.data
    })
  },
})
