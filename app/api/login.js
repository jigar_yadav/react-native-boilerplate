import Settings from '../lib/defaultSettings';
import { coreAPI } from './core';
import moment from 'moment';

export default {
  Login: (data, cb) => {
    let url = 'oauth/token';
    let noti_Url = 'user/me/app';
    let type = 'application/x-www-form-urlencoded', token = null;
    let formData = encodeFormData(data);

    coreAPI.POST(url, type, token, formData).then((res) => {
      let date = new Date();
      res['expires_date'] = moment(date).add((res.expires_in - 60), 'second').toDate();
      coreAPI.GET(noti_Url,res.access_token).then((result)=>{
        if(result.code==200){
          res['notifaction'] = result.data;
        }
        cb(res)
      })
    }).catch((error) => {
      cb(error);
    })
  },

  ForgotPassword: (data, cb) => {
    let url = 'password/reset';
    let type = 'application/json', token = null;

    coreAPI.POST(url, type, token, data).then((res) => {
      cb(res)
    }).catch((error) => {
      cb(error);
    })
  },

  RefreshToken: (data) => {
    return new Promise((resolve, reject) => {
      let url = 'oauth/token';
      let type = 'application/x-www-form-urlencoded', token = null;
      const bodyData = {
          grant_type: 'refresh_token',
          client_id: 'kuro',
          client_secret: 'puty',
          refresh_token: data.refresh_token
        }

      let formData = encodeFormData(bodyData);

      coreAPI.POST(url, type, token, formData).then((res) => {
        let date = new Date();
        res['expires_date'] = moment(date).add((res.expires_in - 60), 'second').toDate();

        resolve(res)
      }).catch((error) => {
        reject(error);
      })
    })
  },
}

encodeFormData = (data) => {
  var formData = [];
  for (var property in data) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(data[property]);
    formData.push(encodedKey + "=" + encodedValue);
  }
  formData = formData.join("&");
  return formData;
}
