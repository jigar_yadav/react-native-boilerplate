//devlopment
var rawUrl = 'https://xyz.com/raw/v1';
var miscUrl = 'https://xyz.com/misc/v1';
var coreUrl = 'https://xyz.com/core/v1';

//production
// var rawUrl = 'https://live.xyz.com/raw/v1';
// var miscUrl = 'https://live.xyz.com/misc/v1';
// var coreUrl = 'https://live.xyz.com/core/v1';


export const coreAPI = {
  GET: (url, token) => {
    return new Promise((resolve, reject) => {
      fetch(coreUrl + url, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token,
          "Access-Control-Allow-Origin":"*",
          'Origin':'https://www.xyz.net',
          "Access-Control-Allow-Headers":"*"
        },
      }).then((response) => response.json())
      .then((responseText) => {
        resolve(responseText)
      })
      .catch((error) => {
        reject(error)
      });
    })
  },

  POST: (url, type, token, data) => {
    return new Promise((resolve, reject) => {
      fetch(coreUrl + url, {
        method: 'POST',
        headers: {
          'Content-Type': type,
          'Authorization': 'Bearer ' + token,
          "Access-Control-Allow-Origin":"*",
          'Origin':'https://www.obiplus.net',
          "Access-Control-Allow-Headers":"*"
        },
        body: data,
      }).then((response) => response.json())
      .then((responseText) => {
        resolve(responseText);
      })
      .catch((error) => {
        reject(error);
      });
    })
  },

  PATCH: (url, type, token, data) => {
    return new Promise((resolve, reject) => {
      fetch(coreUrl + url, {
        method: 'PATCH',
        headers: {
          'Authorization': 'Bearer ' + token,
          'Content-Type': type,
          "Access-Control-Allow-Origin":"*",
          'Origin':'https://www.xyz.net',
          "Access-Control-Allow-Headers":"*"
        },
        body: JSON.stringify(data),
      }).then((response) => response.json())
      .then((responseText) => {
        resolve(responseText);
      })
      .catch((error) => {
        reject(error);
      });
    })
  },

  DELETE: (url, token) => {
    return new Promise((resolve, reject) => {
      fetch(coreUrl + url, {
        method: 'DELETE',
        headers: {
          'Authorization': 'Bearer ' + token,
          "Access-Control-Allow-Origin":"*",
          'Origin':'https://www.xyz.net',
          "Access-Control-Allow-Headers":"*"
        },
      }).then((response) => response.json())
      .then((responseText) => {
        resolve(responseText);
      })
      .catch((error) => {
        reject(error);
      });
    })
  }
};
