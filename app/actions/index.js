import * as demoAction from './demo';
import * as navigationActions from './navigation';
import * as clearStoreAction from './clearStore';

export const ActionCreators = Object.assign({},
  demoAction,
  navigationActions,
  clearStoreAction
);
