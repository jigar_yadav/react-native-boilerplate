export function clearStore() {
  return (dispatch, getState) => {
    dispatch({
      type: types.RESET,
      initialStateComesFrom: {navReducer: {index: 0, routes: [{routeName: "Login", key: "Init0"}]}}
    })
  }
}
