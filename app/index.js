import React, { PropTypes } from 'react';
import {
  Text,
  View,
  AppState,
  AsyncStorage,
  StatusBar,
  Platform
} from 'react-native';

//all imports related to redux
import { Provider, connect } from 'react-redux';
import { createStore, applyMiddleware, compose } from 'redux';
import { createLogger } from 'redux-logger';
import thunkMiddleware from 'redux-thunk';
import reducer from './reducer';
//end of importing redux

//only use to reset whole redux store
import reduxReset from 'redux-reset';

//language import eg. I18n.t('car_header')
import I18n , { refreshLanguage  }  from './i18n/i18n';
//end of language import

const loggerMiddleware = createLogger({ predicate: () => __DEV__ });


//config of store with some middleware  reduxReset is use to reset store
function configureStore(initialState) {
  const enhancer = compose(
    applyMiddleware(
      thunkMiddleware,
      loggerMiddleware,
    ),
    reduxReset({
      type:'RESET',
      data: 'initialStateComesFrom'
    })
  );
  return createStore(reducer, initialState, enhancer);
}
const store = configureStore({});

//importing navigation
import MainNavigation from './navigation/MainNavigation.js';

class MainApp extends React.Component {
  constructor(){
    super();
    this.state = {
      isStoreLoading: false,
      store: store,
    }
  }

  componentWillMount() {
    var self = this;
    // redux store change event listener
    AppState.addEventListener('change', this._handleAppStateChange.bind(this));
    this.setState({isStoreLoading: true});

    AsyncStorage.getItem('YOUR_KEY_store').then((value)=>{
      if(value && value.length){
        let initialStore = JSON.parse(value)
        // initialStore.navReducer.index = 0;
        // initialStore.navReducer.routes = [{type: undefined, routeName: "SplashScreen", key: "Init-0"}];
        self.setState({store: configureStore(initialStore)});
      }else{
        self.setState({store: store});
      }
      self.setState({isStoreLoading: false});
    }).catch((error)=>{
      console.error(error);
      self.setState({store: store});
      self.setState({isStoreLoading: false});
    })
  }

  // event to update current state
  _handleAppStateChange(currentAppState) {
    let initialStore = this.state.store.getState();
    let storingValue = JSON.stringify(initialStore)
    AsyncStorage.setItem('YOUR_KEY_store', storingValue);
  }

  componentWillUnmount() {
    AppState.removeEventListener('change', this._handleAppStateChange.bind(this));
  }

  render () {
    if(this.state.isStoreLoading){
      return(
        <View>
          <Text>Store is loading ... </Text>
        </View>
      )
    }else{
      return(
        <Provider store={this.state.store}>
          <View style={{flex:1}}>
            <StatusBar
              translucent
              backgroundColor="rgba(0, 0 , 0, 0.3)"
              barStyle={ Platform.OS == 'android' ? 'light-content' : 'dark-content' }
              />
            <MainNavigation />
          </View>
        </Provider>
      )
    }
  }
}

export default MainApp;
